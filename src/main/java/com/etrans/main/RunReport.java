package com.etrans.main;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import com.etrans.dao.DBOperations;

public class RunReport {

	public static void main(String[] args) {

		String filelocation = "/opt/etrans/mail_outbox/";
		DBOperations dbo = new DBOperations();
		HashMap<String, Object> devicelistdtls = dbo.getDeviceLists();
		List<String[]> devicelists = (List<String[]>) devicelistdtls.get("devicelistsdtls");
		if (devicelists != null && devicelists.size() > 1) {
			try {
				HashMap<String, String> mobileNos = dbo.mobileNos((String) devicelistdtls.get("devices"));
				String filename = "Concox_Millage_Error_"
						+ (new SimpleDateFormat("dd-MM-yyyy_HH:mm")).format(new java.util.Date()) + ".csv";
				File file = new File(filelocation + filename);
				if (!file.exists()) {
					file.createNewFile();
				}
				FileWriter filewriter = new FileWriter(file, true);
				String devicedtls[];
				StringBuffer data = new StringBuffer();
				for (int i = 0; i < devicelists.size(); i++) {
					devicedtls = devicelists.get(i);
					for (int j = 0; j < devicedtls.length; j++) {
						data.append(devicedtls[j] + ",");
					}
					if (i != 0) {
						if (mobileNos.isEmpty()) {
							data.append("NA");
						} else {
							data.append(mobileNos.get(devicedtls[0]));
						}
						filewriter.write("\n" + data.toString());
					} else {
						filewriter.write("\n" + data.substring(0, (data.length() - 1)));
					}
					data.delete(0, data.length());
				}
				filewriter.close();
				dbo.savetomailoutbox(filename, filelocation);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}
}