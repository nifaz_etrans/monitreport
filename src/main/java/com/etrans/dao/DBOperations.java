package com.etrans.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBOperations {

	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	public HashMap<String, Object> getDeviceLists() {
		HashMap<String, Object> hmap = new HashMap<String, Object>();
		List<String[]> devicelistsdtls = new ArrayList<String[]>();
		String query = "SELECT * FROM FN_DEVICE_LIST()";
		try {
			con = new DBConnection().connectToMonitDatabase();
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			ResultSetMetaData rsm = rs.getMetaData();
			int columncount = rsm.getColumnCount();
			String[] devicehead = new String[(columncount + 1)];
			for (int i = 1; i <= columncount; i++) {
				devicehead[(i - 1)] = rsm.getColumnLabel(i).toUpperCase();
			}
			devicehead[columncount] = "MOBILE_NO";
			devicelistsdtls.add(devicehead);
			String devices = "";
			while (rs.next()) {
				String[] devicedts = new String[columncount];
				for (int i = 1; i <= columncount; i++) {
					if (i == 1) {
						devices = devices + "'" + rs.getString(i) + "',";
					}
					if (rs.getString(i) != null) {
						devicedts[(i - 1)] = rs.getString(i);
					} else {
						devicedts[(i - 1)] = "NA";
					}
				}
				devicelistsdtls.add(devicedts);
			}
			if (devices.length() > 0) {
				hmap.put("devices", devices.substring(0, (devices.length() - 1)));
			}
			hmap.put("devicelistsdtls", devicelistsdtls);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return hmap;
	}

	public HashMap<String, String> mobileNos(String devices) {

		HashMap<String, String> hmap = new HashMap<String, String>();

		String query = "select s_device_id , i_sim_mob_no from syn_month_service where s_device_id in (" + devices
				+ ")";
		try {
			con = new DBConnection().connectToDatabase();
			pst = con.prepareStatement(query);
			System.out.println(pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				hmap.put(rs.getString(1), rs.getString(2));
			}
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		return hmap;
	}

	public void savetomailoutbox(String file_name, String filelocation) {

		String query = "insert into syn_mail_outbox(i_mail_out_seq, i_user_id, s_entity_name, i_entity_id, s_user_name, s_mail_to, s_mail_cc, s_mail_subject, s_mail_body, c_is_attached, s_filename, s_file_path, dt_report, c_is_active, s_created_by, dt_created, c_is_deleted, c_is_processed) values(nextval('syn_seq_mail_outbox'),'100', 'eTrans', 100, 'eTrans', 'kmanohar@etranssolutions.com', 'nifaz@etranssolutions.com', 'Concox Mileage Error', 'This is a computer generated mail. Please do not reply.', 'Y', '"
				+ file_name + "', '" + filelocation
				+ "', CURRENT_TIMESTAMP - INTERVAL '1 HOUR', 'Y', 'SYS', CURRENT_TIMESTAMP, 'N', 'N')";
		try {
			con = new DBConnection().connectToDatabase();
			pst = con.prepareStatement(query);
			pst.executeUpdate();
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				pst.close();
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

}
