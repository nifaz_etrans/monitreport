package com.etrans.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	Connection con = null;

	public Connection connectToMonitDatabase() {
		Connection conn = null;

		try {

			Class.forName("org.postgresql.Driver");
			//String url = "jdbc:postgresql://master:5432/etrans";// developement
			String url = "jdbc:postgresql://monhost:5432/capture";// production
			System.out.println("url: " + url);
			conn = DriverManager.getConnection(url, "capture", "adm123");// production
			//conn = DriverManager.getConnection(url, "schedulejob", "schedulejob@eTrans");// developement
			System.out.println("Connection done To Monit DB!!");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return conn;
	}

	public Connection connectToDatabase() {
		Connection conn = null;

		try {

			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://master:5432/etrans";
			System.out.println("url: " + url);
			conn = DriverManager.getConnection(url, "schedulejob", "schedulejob@eTrans");
			System.out.println("Connection done To etrans DB!!");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return conn;
	}
}
